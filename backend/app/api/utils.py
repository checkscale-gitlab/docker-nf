import numpy as np, requests, json
def do_something(res):
    return {'i_haz_result': res}

def predict_model():
    import tensorflow as tf, numpy as np, os
    model_file = os.path.join('/app/models/', 'model.h5')
    custom = {}
    print('here')
    model = tf.keras.models.load_model(model_file)

    print('\n\n\n\n', model)

    dummy_input = np.zeros((28, 28)).reshape((-1, 28, 28))
    predicted   = model.predict(dummy_input)
    # predicted = None
    print(dummy_input, predicted)
    return {
        'results': predicted.tolist()
    }

def predict_served_model():
    dummy_input = np.zeros((28, 28)).reshape((-1, 28, 28))
    dummy_serve = {"instances": dummy_input.tolist()}
    model_name  = 'toy'
    model_dir = 'models'

    # http://localhost:8501/v1/models/toy
    model_host = 'http://localhost'
    model_host = 'http://tf'
    model_port = '8501'
    model_vers = 1
    model_uri = '{}:{}/v{}/{}/{}:predict'.format(model_host, model_port, model_vers, model_dir, model_name)


    r = requests.post(url=model_uri, data=json.dumps(dummy_serve))
    result_json = r.json()
    print(r, result_json)
    return result_json
