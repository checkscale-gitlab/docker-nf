# Custom Library

This is a custom python library. Depending on the nature of the project, it may
not be uploaded or publicly accessible via pypi or GitLab. Since I am bad
with Docker, the project is put here, and installed  via `pip install -e`
in the `backened/Dockerfile`
